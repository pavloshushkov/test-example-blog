from django.shortcuts import render, get_object_or_404
from django.db.models import ObjectDoesNotExist
from django.http import Http404

from . import models

# Create your views here.
def index(request):
    try:
        posts = models.Post.objects.all()
    except ObjectDoesNotExist:
        raise Http404

    context = {
        'posts' : posts
    }
    return (render(request, 'blog/__base.html', context))

def post_detail(request, pk):
    post = get_object_or_404(models.Post, pk=pk)

    context = {
        'post': post
    }
    return render(request, 'blog/post_detail.html', context )

def about_me(request):
    return render(request, 'blog/about_me.html')

def contacts(request):
    return render(request, 'blog/contacts.html')