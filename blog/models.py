from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=120)
    sub_title = models.CharField(max_length=120, null=True, blank=True)
    text = RichTextUploadingField(blank=True, null=True)
    main_img = models.ImageField(upload_to = 'post_img/',
                                 default = 'img/home-bg.jpg',
                                 null=True,
                                 blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return (self.title)