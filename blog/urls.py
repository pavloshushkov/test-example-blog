from django.conf.urls import url, include

from blog import views
urlpatterns = [
    url(r'^$', views.index, name="index.html"),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^about', views.about_me, name='about_me'),
    url(r'^contacts', views.contacts, name='contacts'),
]